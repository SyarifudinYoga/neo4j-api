const neo4j = require('neo4j-driver');
require('dotenv').config()
const {
    url,
    db_username,
    db_password,
    database,
} = process.env

const driver = neo4j.driver(url, neo4j.auth.basic(db_username, db_password));

const session = driver.session({
    database
});

const findAll = async () =>{
    const result = await session.run(`Match (n:Test) return n`)
    return result.records.map(i=>i.get('n').properties)
}

const findByName = async (name) =>{
  const result = await session.run(`MATCH (n:Test {name : '${name}'} ) return n limit 1`)
  return result.records[0].get('n').properties
}

const postUser = async (user) =>{
  await session.run(`CREATE (n:Test {name: '${user.name}', born: '${user.born}'} ) return n`)
  return await findByName(user.name)
}

const findByNameAndUpdate = async (name, user) =>{
  const result = await session.run(`MATCH (n:Test {name : '${name}'}) SET n.born= '${user.born}' return n`)
  return result.records[0].get('n').properties
}

const findByNameAndDelete = async (name) =>{
  await session.run(`MATCH (n:Test {name : '${name}'}) DELETE n`)
  return await findAll()
}

module.exports = {
    findAll,
    findByName,
    postUser,
    findByNameAndUpdate,
    findByNameAndDelete
}