const Router = require("express")
const { findAll, findByName, postUser, findByNameAndUpdate, findByNameAndDelete } = require('../models/user');

const user = Router()

user.get('/', async (req,res)=>{
    const result = await findAll()
     res.json(result)
})
user.get('/:name', async (req,res)=>{
    const result = await findByName(req.params.name)
    res.json(result)
})
user.post('/', async (req,res)=>{
    const result = await postUser(req.body)
    res.json(result)
})
user.put('/:name', async (req,res)=>{
    const result = await findByNameAndUpdate(req.params.name, req.body)
    res.json(result)
})
user.delete('/:name', async (req,res)=>{
    const result = await findByNameAndDelete(req.params.name)
    res.json(result)
})

module.exports = user