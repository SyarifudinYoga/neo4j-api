const express = require('express');
const user = require('./src/routes/user')

require('dotenv').config()
const app = express()
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use('/',user)
app.listen(process.env.PORT)